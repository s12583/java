package com.tv_class;

import java.util.ArrayList;
import java.util.List;


public class TvSeries
{
    private int id;
    private String name;
    private List<Season> seasons;
    private Director director;

    public TvSeries(int id, String name, List<Season> se, Director director)
    {
        this.seasons = new ArrayList();
        
        se.forEach((e) ->{this.seasons.add(e);});
        this.setIdEpisode();
        
        this.id = id;
        this.name = name;
        this.director = director;
        
        
    }
    
    private void setIdEpisode()
    {
        this.seasons.forEach(a -> a.setIDTVSERIES(id));

    }
    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Season> getSeasons() {
        return seasons;
    }

    public void setSeasons(List<Season> seasons) {
        this.seasons = seasons;
    }

    public Director getDirector()
    {
        return director;
    }

    public void setDirector(Director director)
    {
        this.director = director;
    }
    
    

    
}


