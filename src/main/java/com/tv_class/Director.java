package com.tv_class;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Director 
{
 private int ID;
 private String name; 
 private Date dateofBirth;
 private String biography;

    public Director(int id, String name, String dateofBirth, String biography)
    {
     try
     {
         this.ID =id;
         this.name = name;
         this.dateofBirth = new SimpleDateFormat("yyyy-MM-dd").parse(dateofBirth);
         this.biography = biography;
         
     } catch (ParseException ex)
     {
         Logger.getLogger(Director.class.getName()).log(Level.SEVERE, null, ex);
         System.out.println("Błędna data");
     }
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public Date getDateofBirth()
    {
        return dateofBirth;
    }

    public void setDateofBirth(Date dateofBirth)
    {
        this.dateofBirth = dateofBirth;
    }

    public String getBiography()
    {
        return biography;
    }

    public void setBiography(String biography)
    {
        this.biography = biography;
    }

    public int getID()
    {
        return ID;
    }

    public void setID(int ID)
    {
        this.ID = ID;
    }
 
 
 
}
