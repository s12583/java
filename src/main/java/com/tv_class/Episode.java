package com.tv_class;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Episode
{

    private int ID;
    private String name;
    private Date realaseDate;
    private int episodeNumber;
    private Duration duration;
    private int IDSEASON;

    public Episode(int ID, String name, String realaseDate, int episodeNumber, Duration duration)
    {

        try
        {
            this.ID = ID;
            this.name = name;
            this.realaseDate = new SimpleDateFormat("yyyy-MM-dd").parse(realaseDate);
            this.episodeNumber = episodeNumber;
            this.duration = duration;
            
        } catch (ParseException ex)
        {
            Logger.getLogger(Episode.class.getName()).log(Level.SEVERE, null, ex);
        }
        

    }

    public int getID()
    {
        return ID;
    }

    public void setID(int ID)
    {
        this.ID = ID;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public Date getRealaseDate()
    {
        return realaseDate;
    }

    public void setRealaseDate(Date realaseDate)
    {
        this.realaseDate = realaseDate;
    }

    public int getEpisodeNumber()
    {
        return episodeNumber;
    }

    public void setEpisodeNumber(int episodeNumber)
    {
        this.episodeNumber = episodeNumber;
    }

    public Duration getDuration()
    {
        return duration;
    }

    public void setDuration(Duration duration)
    {
        this.duration = duration;
    }

    public int getIDSEASON()
    {
        return IDSEASON;
    }

    public void setIDSEASON(int IDSEASON)
    {
        this.IDSEASON = IDSEASON;
    }

}
