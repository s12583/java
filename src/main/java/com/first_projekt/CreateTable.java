/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.first_projekt;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;


public class CreateTable
{

    private Statement stmt;
    
    private static final String tvSeriesTable = "CREATE TABLE TVSERIES"
            + "(ID INTEGER NOT NULL IDENTITY,"
            + "NAME VARCHAR(100),"
            + "DIRECTOR INTEGER,"
            + "CONSTRAINT TVSERIES_PK PRIMARY KEY (ID))";

    private static final String directorTable = "CREATE TABLE DIRECTOR"
            + "(ID INTEGER NOT NULL IDENTITY,"
            + "NAME VARCHAR(100),"
            + "DAYOFBIRTH DATE,"
            + "BIOGRAPHY VARCHAR(255),"
            + "CONSTRAINT DIRECTOR_PK PRIMARY KEY (ID))";

    private static final String actorTable = "CREATE TABLE ACTOR"
            + "(ID INTEGER NOT NULL IDENTITY,"
            + "NAME VARCHAR(100),"
            + "DAYOFBIRTH DATE,"
            + "BIOGRAPHY VARCHAR(255),"
            + "ID_TVSERIES INTEGER,"
            + "CONSTRAINT ACTOR_PK PRIMARY KEY (ID))";

    private static final String seasonTable = "CREATE TABLE SEASON"
            + "(ID INTEGER NOT NULL IDENTITY,"
            + "SEASONNUMBER INTEGER,"
            + "YEAROFREALASE INTEGER,"
            + "TVSERIES INTEGER,"
            + "CONSTRAINT SEASON_PK PRIMARY KEY (ID))";

    private static final String episodeTable = "CREATE TABLE EPISODE"
            + "(ID INTEGER NOT NULL IDENTITY,"
            + "NAME VARCHAR(100),"
            + "REALASEDATE DATE,"
            + "EPISODENUMBER INTEGER,"
            + "DURATION TIME,"
            + "IDSEASON INTEGER,"
            + "CONSTRAINT EPISODE_PK PRIMARY KEY (ID))";

    public CreateTable(Connection con) throws SQLException
    {
        try
        {
            this.stmt = con.createStatement();
         

        } catch (SQLException e)
        {
            System.out.println("Brak polaczenia");
            e.printStackTrace(System.out);
        }

    }

    public void setTvSeriesTable() throws SQLException
    {

        try
        {

            stmt.executeUpdate(tvSeriesTable);
            stmt.executeUpdate(directorTable);
            stmt.executeUpdate(actorTable);
            stmt.executeUpdate(seasonTable);
            stmt.executeUpdate(episodeTable);

        } catch (SQLException e)
        {
            e.printStackTrace(System.out);
            
        }
        System.out.println("Tabela utworzona pomyślnie");
    }
    
    

}
