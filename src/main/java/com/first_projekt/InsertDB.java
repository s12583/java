/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.first_projekt;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalTime;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.tv_class.*;


public class InsertDB
{

    private Statement stmt;
    private Connection con;

    protected PreparedStatement addActorStmt;
    protected PreparedStatement addDirectorStmt;
    protected PreparedStatement addEpisodeStmt;
    protected PreparedStatement addSeasonStmt;
    protected PreparedStatement addTvSeriesStmt;

    final static String INSERT_ACTOR = "insert into ACTOR " + " values (?,?,?,?,?)";
    final static String INSERT_DIRECTOR = "insert into DIRECTOR " + " values (?,?,?,?)";
    final static String INSERT_EPISODE = "insert into EPISODE " + " values (?,?,?,?,?,?)";
    final static String INSERT_SEASON = "insert into SEASON " + " values (?,?,?,?)";
    final static String INSERT_TVSERIES = "insert into TVSERIES " + " values (?,?,?)";

    public InsertDB(Connection con)
    {
        try
        {
            this.con = con;
            this.stmt = con.createStatement();

            setAddActorStmt(this.con);
            setAddDirectorStmt(this.con);
            setAddEpisodeStmt(this.con);
            setAddSeasonStmt(this.con);
            setAddTvSeries(this.con);

        } catch (SQLException e)
        {
            System.out.println("Brak polaczenia");
            e.printStackTrace(System.out);
        }
    }

    public void insertActor(Actor actor)
    {
        java.sql.Date sqlDate = new java.sql.Date(actor.getDateofBirth().getTime());

        try
        {
            addActorStmt.setInt(1, actor.getID());
            addActorStmt.setString(2, actor.getName());
            addActorStmt.setDate(3, sqlDate);
            addActorStmt.setString(4, actor.getBiography());
            addActorStmt.setInt(5, actor.getID_TVSERIES());

            addActorStmt.executeUpdate();
            System.out.println("Dodano poprawnie : " + actor.getName());

        } catch (SQLException ex)
        {
            System.out.println("Nie dodano do bazy Actora ");
            Logger.getLogger(InsertDB.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void inserDirector(Director director)
    {
        java.sql.Date sqlDate = new java.sql.Date(director.getDateofBirth().getTime());

        try
        {
            addDirectorStmt.setInt(1, director.getID());
            addDirectorStmt.setString(2, director.getName());
            addDirectorStmt.setDate(3, sqlDate);
            addDirectorStmt.setString(4, director.getBiography());

            addDirectorStmt.executeUpdate();
            System.out.println("Dodano poprawnie : " + director.getName());

        } catch (SQLException ex)
        {
            System.out.println("Nie dodano do bazy dyrektor: ");
            Logger.getLogger(InsertDB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void inserEpisode(Episode episode)
    {
        LocalTime localTime = LocalTime.MIDNIGHT.plus(episode.getDuration());
        java.sql.Time sqlTime = java.sql.Time.valueOf(localTime);

        java.sql.Date sqlDate = new java.sql.Date(episode.getRealaseDate().getTime());

        try
        {
            addEpisodeStmt.setInt(1, episode.getID());
            addEpisodeStmt.setString(2, episode.getName());
            addEpisodeStmt.setDate(3, sqlDate);
            addEpisodeStmt.setInt(4, episode.getEpisodeNumber());
            addEpisodeStmt.setTime(5, sqlTime);
            addEpisodeStmt.setInt(6, episode.getIDSEASON());

            addEpisodeStmt.executeUpdate();
            System.out.println("Dodano poprawnie " + episode.getName());

        } catch (SQLException ex)
        {
            System.out.println("Nie dodano do bazy episodu ");
            Logger.getLogger(InsertDB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void inserSeason(Season season)
    {
       /* if (!season.getEpisodes().isEmpty())
        {

            season.getEpisodes().forEach((e) ->
            {
                this.inserEpisode(e);
            });
        } else
        {
            System.out.println("Sezon " + season.getID() + "nie zawiera zadnych epizodow");
        }
        */
        try
        {
            addSeasonStmt.setInt(1, season.getID());
            addSeasonStmt.setInt(2, season.getSeasonNumber());
            addSeasonStmt.setInt(3, season.getYearOfRealease());
            addSeasonStmt.setInt(4, season.getIDTVSERIES());

            addSeasonStmt.executeUpdate();
            System.out.println("Dodano poprawnie " + season.getSeasonNumber());

        } catch (SQLException ex)
        {
            System.out.println("Nie dodano do bazy");
            Logger.getLogger(InsertDB.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void inserTvSeries(TvSeries tvseries)
    {
        Boolean empty_episode = true;

        if (!tvseries.getSeasons().isEmpty()) // dodaje sezony do tabeli
        {

            tvseries.getSeasons().forEach((e) ->
            {
                this.inserSeason(e);
            });

        } else
        {
            System.out.println("Sezon " + tvseries.getId() + "nie zawiera zadnych sezonow");
        }

        for (Season s : tvseries.getSeasons()) 
        {
            empty_episode = s.getEpisodes().isEmpty();
        }

        if (!empty_episode) // dodaje episody do tabeli 
        {

            tvseries.getSeasons().forEach((s) ->
            {

                s.getEpisodes().forEach(e -> this.inserEpisode(e)
                );

            });

        } else
        {
            System.out.println("Sezon " + tvseries.getId() + "nie zawiera zadnych epizodow");
        }

        try
        {
            addTvSeriesStmt.setInt(1, tvseries.getId());
            addTvSeriesStmt.setString(2, tvseries.getName());
            addTvSeriesStmt.setInt(3, tvseries.getDirector().getID());

            addTvSeriesStmt.executeUpdate();

            System.out.println("Dodano poprawnie " + tvseries.getName());

        } catch (SQLException ex)
        {
            System.out.println("Nie dodano do bazy tv SERIES ");
            Logger.getLogger(InsertDB.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void setAddTvSeries(Connection con)
    {
        try
        {
            this.addTvSeriesStmt = con.prepareStatement(INSERT_TVSERIES);

        } catch (SQLException ex)
        {
            Logger.getLogger(InsertDB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void setAddActorStmt(Connection con)
    {
        try
        {
            this.addActorStmt = con.prepareStatement(INSERT_ACTOR);

        } catch (SQLException ex)
        {
            Logger.getLogger(InsertDB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void setAddDirectorStmt(Connection con)
    {
        try
        {
            this.addDirectorStmt = con.prepareStatement(INSERT_DIRECTOR);

        } catch (SQLException ex)
        {
            Logger.getLogger(InsertDB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void setAddEpisodeStmt(Connection con)
    {

        try
        {
            this.addEpisodeStmt = con.prepareStatement(INSERT_EPISODE);

        } catch (SQLException ex)
        {
            Logger.getLogger(InsertDB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void setAddSeasonStmt(Connection con)
    {

        try
        {
            this.addSeasonStmt = con.prepareStatement(INSERT_SEASON);

        } catch (SQLException ex)
        {
            Logger.getLogger(InsertDB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
