/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.first_projekt;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;


public class SelectDB
{

    private static Scanner in;
    private Connection con;
    private Statement stat;

   // private List<String> 

    private static String queryActor = "SELECT * FROM ACTOR";
    private static String queryDirector = "SELECT * FROM DIRECTOR";
    private static String queryEpisode = "SELECT * FROM EPISODE";
    private static String querySeason = "SELECT * FROM SEASON";
    private static String queryTvSeries = "SELECT TVSERIES.NAME, DIRECTOR.NAME FROM TVSERIES INNER JOIN DIRECTOR ON TVSERIES.DIRECTOR = DIRECTOR.ID" ;
    // private static String queryTvSeriesAll = "SELECT * FROM TVSERIES, DIRECTOR, ACTOR, SEASON, EPISODE WHERE TVSERIES.ID = 1 DIRECTOR.ID = TVSERIES.DIRECTOR SEASON.TVSERIES = TVSERIES.ID EPISODE.IDSEASON = SEASON.ID";
    

    public SelectDB(Connection con)
    {
        

        try
        {
            this.stat = con.createStatement();
            this.con = con;

        } catch (SQLException ex)
        {
            Logger.getLogger(SelectDB.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void showTableActor()
    {
        List<String> actor = new ArrayList();
        
        try (ResultSet rs = stat.executeQuery(queryActor))
        {

            while (rs.next())
            {

                actor.add("\n \t Nazwa : " + rs.getString(2) 
                        + " Data Urodzin : " +      rs.getString(3) 
                        + " Biografia  : " +        rs.getString(4)); 


            }

        } catch (SQLException ex)
        {
            Logger.getLogger(SelectDB.class.getName()).log(Level.SEVERE, null, ex);
        }

        System.out.print(actor);

    }
    
    public void showTableDirector()
    {
        List<String> director = new ArrayList();
        
        try (ResultSet rs = stat.executeQuery(queryDirector))
        {

            while (rs.next())
            {

                director.add("\n \t Nazwa : " + rs.getString(2) 
                        + " Data Urodzin : " +      rs.getString(3) 
                        + " Biografia  : " +        rs.getString(4)); 


            }

        } catch (SQLException ex)
        {
            Logger.getLogger(SelectDB.class.getName()).log(Level.SEVERE, null, ex);
        }

        System.out.print(director);

    }
    
    public void showTableEpisode()
    {
        List<String> episode = new ArrayList();
        
        try (ResultSet rs = stat.executeQuery(queryEpisode))
        {

            while (rs.next())
            {

                episode.add("\n \t Nazwa : "+rs.getString(2)+ 
                        " Data : "+ rs.getString(3)+ 
                        " Sezon : "+rs.getString(4)+ 
                        " Czas : "+ rs.getString(5)); 


            }

        } catch (SQLException ex)
        {
            Logger.getLogger(SelectDB.class.getName()).log(Level.SEVERE, null, ex);
        }

        System.out.print(episode);

    }
    
    public void showTableSeason()
    {
        List<String> season = new ArrayList();
        
        try (ResultSet rs = stat.executeQuery(querySeason))
        {

            while (rs.next())
            {

                season.add("\n \t Numer sezonu : "+rs.getString(2)+ 
                        " Data wydania : "+ rs.getString(3)
                        ); 


            }

        } catch (SQLException ex)
        {
            Logger.getLogger(SelectDB.class.getName()).log(Level.SEVERE, null, ex);
        }

        System.out.print(season);

    }
    
    public void showTableTvSeries()
    {
        List<String> tvseries = new ArrayList();
        
        try (ResultSet rs = stat.executeQuery(queryTvSeries))
        {

            while (rs.next())
            {

                tvseries.add("\n \t Nazwa serialu : "+rs.getString("NAME")+ 
                        " Reżyser : "+ rs.getString(2)
                        ); 


            }

        } catch (SQLException ex)
        {
            Logger.getLogger(SelectDB.class.getName()).log(Level.SEVERE, null, ex);
        }

        System.out.print(tvseries);

    }
    /*
     public void showTvSeries(int tvseries_ID)
    {
        List<String> tvseries = new ArrayList();
        
        try (ResultSet rs = stat.executeQuery(queryTvSeries))
        {

            while (rs.next())
            {

                tvseries.add("\n \t Nazwa serialu : "+rs.getString("NAME")+ 
                        " Reżyser : "+ rs.getString(2)
                        ); 


            }

        } catch (SQLException ex)
        {
            Logger.getLogger(SelectDB.class.getName()).log(Level.SEVERE, null, ex);
        }

        System.out.print(tvseries);

    }
*/
}
