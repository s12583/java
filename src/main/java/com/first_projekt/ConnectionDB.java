/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.first_projekt;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.SQLWarning;


public class ConnectionDB
{
    private final String URL_DB = "jdbc:hsqldb:hsql://localhost/testdb";
    private final String User = "SA";
    private final String Password = "";
    private final String DriverName = "org.hsqldb.jdbc.JDBCDriver";
    private SQLWarning warn;
    private Connection con;
    
    
    public ConnectionDB()
    {
      try
      {
         Class.forName(DriverName); 
         this.con = DriverManager.getConnection(URL_DB,User,Password);
         if(this.con != null){ System.out.println("Połaczono z baza");}
         else {System.out.println("Problem z baza");} 
      }
      catch (Exception e)
      {
           e.printStackTrace(System.out);
           System.out.println("Problem z baza");
      }
            
    }
    
    public void closeConnection() throws SQLException
    {
        this.con.close();
    }

    public Connection getCon()
    {
        return con;
    }
    
        
    
}
